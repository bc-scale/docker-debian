#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# Check for Docker, we cannot do anything without it
if [ "$(which 'docker' ; echo "${?}")" == '1' ]
then
	echo 'Docker executable was not found, make sure it is installed and in your path' >&2
	exit 1
fi

# Parse the command line
TEMP="$(getopt -n 'debian-container-builder' -o 'cu:n:t:l:i:s:' --long 'use-container,username:,name:,user-tag:,locales:,source-image:,source-tag:,container-name:,uid:' -- "${@}")"
eval set -- "${TEMP}"

while [ ${#} -gt 0 ]
do
	case "${1}" in
	'-c' | '--use-container')
		USE_CONTAINER='1'
		shift
		;;

	'-u' | '--username')
		USERNAME="${2}"
		shift 2
		;;

	'-n' | '--name')
		NAME="${2}"
		shift 2
		;;

	'-t' | '--user-tag')
		USER_TAG="${2}"
		shift 2
		;;

	'-l' | '--locales')
		LOCALES="${2}"
		shift 2
		;;

	'-i' | '--source-image')
		IMAGE_SOURCE="${2}"
		shift 2
		;;

	'-s' | '--source-tag')
		IMAGE_TAG="${2}"
		shift 2
		;;

	'--container-name')
		CONTAINER_NAME="${2}"
		shift 2
		;;

	'--uid')
		CONTAINER_UID="${2}"
		shift 2
		;;

	'--')
		shift
		break
		;;
	esac
done

# Locales to generate
LOCALES="${LOCALES:-"en_US.UTF-8 UTF-8,fi_FI.UTF-8 UTF-8"}"

# User information
CONTAINER_UID="${CONTAINER_UID:-"${UID}"}"
USERNAME="${USERNAME:-"$(whoami)"}"
NAME="${NAME:-"$(getent 'passwd' "${USER}" |cut --delim ':' --field '5' |cut --delim ',' --field '1')"}"

# Container name
CONTAINER_NAME="${CONTAINER_NAME:-"Debian ${USERNAME}"}"

# The source image and tags to use
IMAGE_SOURCE="${IMAGE_SOURCE:-"quay.io/mireiawen/debian"}"
IMAGE_TAG="${IMAGE_TAG:-"latest"}"
USER_TAG="${USER_TAG:-"${USERNAME}"}"

# Use the container itself to build it
USE_CONTAINER="${USE_CONTAINER:-"0"}"

# Pull the base image to use as cache base
docker 'pull' "${IMAGE_SOURCE}:${IMAGE_TAG}" ||  true

# Build the base image
docker 'build' \
	'.' \
	--file 'Dockerfile' \
	--tag "${IMAGE_SOURCE}:${IMAGE_TAG}" \
	"${@}"

# Fix the locales
LOCALES="${LOCALES/,/\\n}"

# Build the user image
if [ "${USE_CONTAINER}" == '0' ]
then
	# Export for Jinja2
	export LOCALES
	export USERNAME
	export NAME
	export CONTAINER_UID
	export CONTAINER_NAME

	j2 'Dockerfile.user.j2' |\
	docker 'build' \
		'.' \
		--tag "${IMAGE_SOURCE}:${USER_TAG}" \
		--file '-' \
		"${@}"
else
	docker 'run' \
		--rm \
		--interactive \
		--tty \
		--volume "$(pwd):/work:ro" \
		--workdir '/work' \
		--env LOCALES="${LOCALES}" \
		--env USERNAME="${USERNAME}" \
		--env NAME="${NAME}" \
		--env CONTAINER_UID="${CONTAINER_UID}" \
		--env CONTAINER_NAME="${CONTAINER_NAME}" \
		"${IMAGE_SOURCE}:${IMAGE_TAG}" \
		-c "j2 'Dockerfile.user.j2'" |\
		docker 'build' \
			'.' \
			--tag "${IMAGE_SOURCE}:${USER_TAG}" \
			--file '-' \
			"${@}"
fi
